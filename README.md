## Prueba Native Mobile Developer

### Lo que se busca

1. Crear una aplicación nativa Android 
2. Crear una aplicación nativa IOS 
3. Hacer un inicio de sesión y validación por API externa 
4. Obtener datos de la API entregando correctamente los 

### Instrucciones y requisitos técnicos 

Octano a creado una API para que puedan obtener datos y validar el inicio de usuarios, todas estas estan en el sitio [http://api-urbana.octano.cl/api](http://api-urbana.octano.cl/api/auth). En específico se pide que 

  

1. Se deben crear dos aplicaciones 100% nativas 
2. Se debe crear una vista Login que pueda hacer inicio de sesion con los siguientes parámetros 
    1. Credenciales para obtener Token 
        1. rut: 222222222 
        2. password: 222222222 

    2. Direccion de la Api 
        1. [http://api-urbana.octano.cl/api/auth](http://api-urbana.octano.cl/api/auth) 

3. Se les pedirá realizar una serie de operaciones con Centros de salud entregados por la API ya creada. Esta contiene los siguientes parámetros en base de datos. 
    1. name (string)(unique) 
    2. address (string) 
    3. latitude (string) 
    4. longitude (string) 
    5. created_at (timestamp) 

4. Para poder utilizar la API de los centros, es necesario que en el Header de cada petición se debe agregar  
    1. Header de validación 
        1. {Authorization: Bearer Token} 

5. Luego deben poder tener una vista para ver un listado de centros 
    1. Direccion de la API 
        1. [http://api-urbana.octano.cl/api/centros](http://api-urbana.octano.cl/api/centros)  =&gt; (get) all 

6. Se debe poder ver el detalle de un centro 
    1. Direccion de la API 
        1. [http://api-urbana.octano.cl/api/centros/:id](http://api-urbana.octano.cl/api/centros/:id)  =&gt; (get) find 

7. Se debe poder Crear y Editar los centros 
    1. Direccion de la API 
        1. [http://api-urbana.octano.cl/api/centros](http://api-urbana.octano.cl/api/centros)  =&gt; (post) 
        2. [http://api-urbana.octano.cl/api/centros/:id](http://api-urbana.octano.cl/api/centros/:id)  =&gt; (put) 

8. Por último se debe poder eliminar un local 
    1. [http://api-urbana.octano.cl/api/centros/:id](http://api-urbana.octano.cl/api/centros/:id)  =&gt; (delete) 

9. Suma puntos si usa gestor de paquetes 
10. Conocimiento de controlador de versiones 
11. Recordar enviar e-mail, al finalizar la prueba. Este será el tiempo controlado 

### Lo que se debe entregar

1. Código fuente, y archivos de instalación de dependencias 
2. Documento Instrucciones de instalación y ejecución  
3. Se debe entregar el código lo más ordenado posible, documentando el código que implique una mayor complejidad de lectura 
4. Se deben entregar dos proyectos uno para cada plataforma, IOS y Android 

### Rubrica de evaluación

1. Cumplimiento de los requerimientos técnicos solicitados 
2. Tiempo de desarrollo, mientras antes es mejor pero al no estar completo se penalizará puntaje 
3. Utilización correcta del framework 
4. Documentación dentro del código, junto a documento técnico de instalación y uso. 
5. Diseño 
6. Vista, edicion, creacion y eliminacion de los centros 
7. Ingreso al usuario y validación de Token 

### Reglas

1. La prueba tiene un deadline a las 20:00 del día 31 de Diciembre 
2. Finalizado el plazo, el postulante deberá subir su trabajo a un fork de un repositorio GIT a especificar más adelante 
3. Cada postulante debe crear una rama propia para subir sus archivos 
4. Los minutos que excedan del deadline restará puntaje